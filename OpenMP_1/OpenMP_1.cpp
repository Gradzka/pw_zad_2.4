// OpenMP_1.cpp : Defines the entry point for the console application.
// Program sekwencyjny

#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "omp.h"
#include "iostream"
#define ZAKRES 1 //lepsze efekty wspolbieznosci dla duzych zakresow
using namespace std;

int* create_vector(int n)
{
	int *vect = new int[n];
	return vect;
}
void destroy_vector(int *vect, int n)
{
	delete vect;
}
void random_vector(int *vect, int n,int zakres)
{
	for (int i = 0; i < n; i++)
		vect[i] = rand() % zakres;
}
void show_vector(int *vect, int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", vect[i]);
	printf("\n\n");
}
void fill_histogram(int n,int *tab,int *hist,double &start,double &end)
{
	int m;
	int zm;
	start = omp_get_wtime();
	for (long k = 0; k < ZAKRES; k++)
	{
	#pragma omp parallel for shared(hist,tab,n) private(m)
		for (m = 0; m < n; m++)
		{
			hist[tab[m]] = hist[tab[m]] +1;
			//cout << omp_get_thread_num();
		}
	}
	end = omp_get_wtime();
}
int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	int *tablica;
	int *histogram;
	int rozmiar = 10;
	int zakres = 20;
	double start, end, result;

	tablica = create_vector(rozmiar);
	random_vector(tablica, rozmiar, zakres);

	histogram = create_vector(zakres);
	random_vector(histogram, zakres, 1); // wypelnienie histogramu 0

	cout << "Elementy tablicy jednowymiarowej:"<<endl;
	show_vector(tablica, rozmiar);
	//show_vector(histogram, zakres);

	cout << "Zaczynam tworzenie histogramu!\n";

	fill_histogram(rozmiar,tablica,histogram,start,end);
	result = end - start;

	cout << "\nCzas wykonania w sekundach "<<result<<endl;
	cout << endl << "Wyswietlenie zawartosci histogramu:"<<endl;
	show_vector(histogram, zakres);
	destroy_vector(histogram,zakres);
	destroy_vector(tablica,rozmiar);
	return 0;
}

